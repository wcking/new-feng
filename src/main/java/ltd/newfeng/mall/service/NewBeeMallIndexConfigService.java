 
package ltd.newfeng.mall.service;

import ltd.newfeng.mall.api.mall.vo.NewBeeMallIndexConfigGoodsVO;
import ltd.newfeng.mall.entity.IndexConfig;
import ltd.newfeng.mall.util.PageQueryUtil;
import ltd.newfeng.mall.util.PageResult;

import java.util.List;

public interface NewBeeMallIndexConfigService {

    /**
     * 返回固定数量的首页配置商品对象(首页调用)
     *
     * @param number
     * @return
     */
    List<NewBeeMallIndexConfigGoodsVO> getConfigGoodsesForIndex(int configType, int number);

    /**
     * 后台分页
     *
     * @param pageUtil
     * @return
     */
    PageResult getConfigsPage(PageQueryUtil pageUtil);

    String saveIndexConfig(IndexConfig indexConfig);

    String updateIndexConfig(IndexConfig indexConfig);

    IndexConfig getIndexConfigById(Long id);

    Boolean deleteBatch(Long[] ids);
}

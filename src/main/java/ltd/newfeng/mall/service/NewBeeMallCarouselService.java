 
package ltd.newfeng.mall.service;

import ltd.newfeng.mall.api.mall.vo.NewBeeMallIndexCarouselVO;
import ltd.newfeng.mall.entity.Carousel;
import ltd.newfeng.mall.util.PageQueryUtil;
import ltd.newfeng.mall.util.PageResult;

import java.util.List;

public interface NewBeeMallCarouselService {

    /**
     * 返回固定数量的轮播图对象(首页调用)
     *
     * @param number
     * @return
     */
    List<NewBeeMallIndexCarouselVO> getCarouselsForIndex(int number);

    /**
     * 后台分页
     *
     * @param pageUtil
     * @return
     */
    PageResult getCarouselPage(PageQueryUtil pageUtil);

    String saveCarousel(Carousel carousel);

    String updateCarousel(Carousel carousel);

    Carousel getCarouselById(Integer id);

    Boolean deleteBatch(Long[] ids);
}
